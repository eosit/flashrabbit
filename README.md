FlashRabbit
===========

Read lightning strike data from Nexstorm and publish to RabbitMQ 

Example
-------

Payload of a typical AMQP message:

```
{
    "station": "ABC",
    "count":532,
    "year":2016,
    "month":10,
    "day":14,
    "seconds":3867,
    "TRACBearing":1203,
    "RawBearing":1203,
    "TRACDistance":387,
    "RawDistance":387,
    "TRACX":639,
    "TRACY":581,
    "StrikeType":0,
    "StrikePolarity":0
}
```

Requirements
------------

* http://www.csscript.net/currentrelease.html
* https://www.rabbitmq.com/dotnet.html


Usage
-----

System.Web.Extensions.dll is needed for JSON serialization.

Run the script using cscs.exe:

```
cscs /r:System.Web.Extensions.dll flashgate.cs
``` 

To read storms rather than strikes:

```
cscs /r:System.Web.Extensions.dll flashgate.cs storms
``` 


Flashgate IPC notes
-------------------

From the Nexstorm user manual:

```
Recommended interval between polls: 15ms

Stored string contains comma-separated values:
0  count
1  year
2  month
3  day
4  timestamp_secs
5  TRACbearing
6  TRACDistance
7  RAWbearing
8  RAWDistance
9  TRAC_X
10 TRAC_Y
11 Correlated strike
12 Reserved
13 StrikeType
14 StrikePolarity

Signal is noise if any bearing or distance has a value of -1
Signal is heartbeat if any parameter excluding timestamp_secs or RAWbearing has a
value of -9 (or 0 for unsigned integer types)
FlashGate IPC-1 heartbeats have the same format as regular data and can be used to monitor the application
health. They occur at approximate intervals of 1.5 minutes so a watchdog type IPC client needs not to
continuously check the shared memory.
In case of a heartbeat pulse, the RAWbearing record will contain antenna rotation (001° to 360°). Any value
between 1 and 359 must be compensated for in the IPC client implementation
```
