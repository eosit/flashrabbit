using System;
using System.IO.MemoryMappedFiles;
using System.Runtime.InteropServices;
using System.Threading;
using System.Security.AccessControl;
using RabbitMQ.Client;
using System.Text;
using System.Web.Script.Serialization;

/*

IPC for Strike data:
--------------------
Recommended interval between polls: 15ms

Stored string contains comma-separated values:
0  count
1  year
2  month
3  day
4  timestamp_secs
5  TRACbearing
6  TRACDistance
7  RAWbearing
8  RAWDistance
9  TRAC_X
10 TRAC_Y
11 Correlated strike
12 Reserved
13 StrikeType
14 StrikePolarity

Signal is noise if any bearing or distance has a value of -1
Signal is heartbeat if any parameter excluding timestamp_secs or RAWbearing has a
value of -9 (or 0 for unsigned integer types)
FlashGate IPC-1 heartbeats have the same format as regular data and can be used to monitor the application
health. They occur at approximate intervals of 1.5 minutes so a watchdog type IPC client needs not to
continuously check the shared memory.
In case of a heartbeat pulse, the RAWbearing record will contain antenna rotation (001° to 360°). Any value
between 1 and 359 must be compensated for in the IPC client implementation
*/

public class StrikeData
{
    public string station;     // station code / identifier
    public uint count;         // n-th message for the day
    public uint year, month, day;
    public uint seconds;       // seconds since midnight
    public int TRACBearing, RawBearing;   // 0-3600
    public int TRACDistance, RawDistance; //km
    public int TRACX, TRACY;   // 0-1000
    public int StrikeType;     // 0=CG 1=IC
    public int StrikePolarity; // 0=Pos 1=Neg
    public bool is_heartbeat;
}

public class StormData
{
    public string station;     // station code / identifier
    public uint count;
    public uint current_timestamp;
    public int detect_year;
    public int detect_month;
    public int detect_day;
    public uint detect_timestamp;
    public char ident_char;
    public int ident_number;
    public uint lastactive_timestamp;
    public int storm_bearing;
    public int storm_distance;
    public int storm_X;
    public int storm_Y;
    public int curr_strikerate;
    public int max_strikerate;
    public int category;
    public int trend;
    public int tot_strikes;
    public int pos_cg_strikes;
    public int neg_cg_strikes;
    public int pos_ic_strikes;
    public int neg_ic_strikes;
    public int conf_close_alarm;
    public int conf_severe_alarm;
    public int conf_cell_alarm;
    public int trig_close_alarm;
    public int trig_severe_alarm;
    public int trig_cell_alarm;
    public bool is_heartbeat;
}

class Program
{
    static string strikeFileName = "NXFGIPC_SHMEM_0822931589443_238731_GATE0";
    static string tracFileName = "NXFGIPC_SHMEM_5216659004766_091723_GATE0";
    static string strikeReaderSemaphoreName = "Reader Semaphore";
    static string strikeWriterSemaphoreName = "Writer Semaphore";
    static string tracReaderSemaphoreName = "Reader Semaphore 2";
    static string tracWriterSemaphoreName = "Writer Semaphore 2";
    static int strikeDataLength = 1024;
    static int tracDataLength = 2048;

    static string stationCode = "{{ station_code }}";
    static string rabbitmqExchange = "{{ rabbitmq_exchange }}";
    static string rkStrike = string.Format("nexstorm.{0}.strike", stationCode);
    static string rkStorm = string.Format("nexstorm.{0}.storm", stationCode);
    static string rkStrikeHeartbeat = string.Format("nexstorm.{0}.heartbeat.strike", stationCode);
    static string rkStormHeartbeat = string.Format("nexstorm.{0}.heartbeat.storm", stationCode);

    static void Main(string[] args)
    {
        bool storms_not_strikes;
        storms_not_strikes = false;
        if (args.Length > 0)
            if (args[0].ToLower() == "storms")
                storms_not_strikes = true;

        Console.WriteLine("Starting ...");
        while (true)
        {
            try
            {
                Program.run(storms_not_strikes);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
            }

            Console.WriteLine("Restarting ...");
        }
    }

    static void run(bool storms_not_strikes)
    {
        var factory = new ConnectionFactory();

        var uri = "{{ rabbitmq_connection_uri }}";
        factory.Uri = uri;
        factory.AutomaticRecoveryEnabled = true;
        Console.WriteLine("Connecting: ...", uri);

        using (var connection = factory.CreateConnection())
        using (var channel = connection.CreateModel())
        using (var semReader = Semaphore.OpenExisting(storms_not_strikes ? tracReaderSemaphoreName : strikeReaderSemaphoreName))
        using (var semWriter = Semaphore.OpenExisting(storms_not_strikes ? tracWriterSemaphoreName : strikeWriterSemaphoreName))
        using (var mmf = MemoryMappedFile.OpenExisting(storms_not_strikes ? tracFileName : strikeFileName, MemoryMappedFileRights.Read))
        {
            var dataLength = storms_not_strikes ? tracDataLength : strikeDataLength;

            using (var accessor = mmf.CreateViewAccessor(0, dataLength, MemoryMappedFileAccess.Read))
            {
                byte[] data = new byte[dataLength];

                int lastCount = -1;

                while (true)
                {
                    try
                    {
                        // Read from Flashgate IPC shared memory
                        semReader.WaitOne();
                        for (int i = 0; i < dataLength; i++)
                        {
                            data[i] = accessor.ReadByte(i);
                            if (data[i] == 0)
                                break;
                        };
                        semReader.Release();

                        // Extract CSV text
                        string text = System.Text.Encoding.ASCII.GetString(data);
                        text = text.Remove(text.IndexOf('\0'));
                        text = text.Trim();

                        // Check if any new data has been received
                        string[] values = text.Split(',');

                        int count = -1;

                        if (int.TryParse(values[0], out count))
                        {
                            if (count != lastCount)
                            {
                                Console.WriteLine("count: {0}", count);
                                Console.WriteLine("text: {0}", text);

                                if (storms_not_strikes)
                                {
                                    var obj = new StormData();

                                    if (
                                        uint.TryParse(values[0], out obj.count) &&
                                        uint.TryParse(values[1], out obj.current_timestamp) &&
                                        int.TryParse(values[2], out obj.detect_year) &&
                                        int.TryParse(values[3], out obj.detect_month) &&
                                        int.TryParse(values[4], out obj.detect_day) &&
                                        uint.TryParse(values[5], out obj.detect_timestamp) &&
                                        int.TryParse(values[7], out obj.ident_number) &&
                                        uint.TryParse(values[8], out obj.lastactive_timestamp) &&
                                        int.TryParse(values[9], out obj.storm_bearing) &&
                                        int.TryParse(values[10], out obj.storm_distance) &&
                                        int.TryParse(values[11], out obj.storm_X) &&
                                        int.TryParse(values[12], out obj.storm_Y) &&
                                        int.TryParse(values[13], out obj.curr_strikerate) &&
                                        int.TryParse(values[14], out obj.max_strikerate) &&
                                        int.TryParse(values[15], out obj.category) &&
                                        int.TryParse(values[16], out obj.trend) &&
                                        int.TryParse(values[17], out obj.tot_strikes) &&
                                        int.TryParse(values[18], out obj.pos_cg_strikes) &&
                                        int.TryParse(values[19], out obj.neg_cg_strikes) &&
                                        int.TryParse(values[20], out obj.pos_ic_strikes) &&
                                        int.TryParse(values[21], out obj.neg_ic_strikes) &&
                                        int.TryParse(values[22], out obj.conf_close_alarm) &&
                                        int.TryParse(values[23], out obj.conf_severe_alarm) &&
                                        int.TryParse(values[24], out obj.conf_cell_alarm) &&
                                        int.TryParse(values[25], out obj.conf_close_alarm) &&
                                        int.TryParse(values[26], out obj.conf_severe_alarm) &&
                                        int.TryParse(values[27], out obj.conf_cell_alarm)
                                    )
                                    {
                                        obj.ident_char = values[6][0];
                                        obj.station = stationCode;
                                        obj.is_heartbeat = (obj.tot_strikes == -9);
                                        var json = new JavaScriptSerializer().Serialize(obj);
                                        Console.WriteLine(json);

                                        // Data is new
                                        var body = Encoding.UTF8.GetBytes(json);

                                        var rk = obj.is_heartbeat ? rkStormHeartbeat : rkStorm;
                                        channel.BasicPublish(exchange: rabbitmqExchange,
                                                         routingKey: rk,
                                                         basicProperties: null,
                                                         body: body);
                                        Console.WriteLine(" [x] Sent with rk={0}", rk);
                                        Console.WriteLine();
                                    }
                                    else
                                    {
                                        Console.WriteLine("Failed to parse CSV!");
                                    }
                                }
                                else
                                {
                                    var obj = new StrikeData();

                                    if (
                                        uint.TryParse(values[0], out obj.count) &&
                                        uint.TryParse(values[1], out obj.year) &&
                                        uint.TryParse(values[2], out obj.month) &&
                                        uint.TryParse(values[3], out obj.day) &&
                                        uint.TryParse(values[4], out obj.seconds) &&
                                        int.TryParse(values[5], out obj.TRACBearing) &&
                                        int.TryParse(values[6], out obj.TRACDistance) &&
                                        int.TryParse(values[7], out obj.RawBearing) &&
                                        int.TryParse(values[8], out obj.RawDistance) &&
                                        int.TryParse(values[9], out obj.TRACX) &&
                                        int.TryParse(values[10], out obj.TRACY) &&
                                        int.TryParse(values[13], out obj.StrikeType) &&
                                        int.TryParse(values[14], out obj.StrikePolarity)
                                    )
                                    {
                                        obj.station = stationCode;
                                        obj.is_heartbeat = (obj.StrikeType == -9);
                                        var json = new JavaScriptSerializer().Serialize(obj);
                                        Console.WriteLine(json);

                                        // Data is new
                                        var body = Encoding.UTF8.GetBytes(json);

                                        var rk = obj.is_heartbeat ? rkStrikeHeartbeat : rkStrike;
                                        channel.BasicPublish(exchange: rabbitmqExchange,
                                                         routingKey: rk,
                                                         basicProperties: null,
                                                         body: body);
                                        Console.WriteLine(" [x] Sent with rk={0}", rk);
                                        Console.WriteLine();
                                    }
                                    else
                                    {
                                        Console.WriteLine("Failed to parse CSV!");
                                    }
                                }
                            }
                        };

                        lastCount = count;

                        // Wait 15ms
                        Thread.Sleep(storms_not_strikes ? 30 : 15);
                    }
                    catch (SemaphoreFullException ex)
                    {
                        Console.WriteLine("Semaphore full: {0}", ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception: {0}", ex.Message);
                    }
                }
            }
        }
    }
}
